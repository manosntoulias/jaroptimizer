#!/bin/bash
# $1 -> times to run the experiment
# run from this(Example) folder

	cd ..
	java -cp build/libs/jaroptimizer-all.jar:Example/proguard.jar optimiser.JarOptimiser -jar Example/proguard.jar -o Example/proguardMOD.jar -i analysis/context_insensitive/proguard/SingleInvocationToInline.csv -d 1 -p inline -r analysis/context_insensitive/proguard/MethodToRemove.csv -s analysis/context_insensitive/proguard/SingleInvocationTarget.csv 

	java -cp build/libs/jaroptimizer-all.jar:Example/proguard.jar optimiser.JarOptimiser -jar Example/proguard.jar -o Example/proguardSTATIC.jar -d 1 -p inline -r analysis/context_insensitive/proguard/MethodToRemove.csv -s analysis/context_insensitive/proguard/SingleInvocationTarget.csv

	java -cp build/libs/jaroptimizer-all.jar:Example/proguard.jar optimiser.JarOptimiser -jar Example/proguard.jar -o Example/proguardINLINE.jar -i analysis/context_insensitive/proguard/SingleInvocationToInline.csv -d 1 -p inline -r analysis/context_insensitive/proguard/MethodToRemove.csv

check_size () {
	size=$(stat --printf="%s\n" ./proguard_out.jar)
	if [[ $size != '573821' ]]
	then
		echo $1" ERROR: size is "$size". Must be 573821" 1>&2
	fi		
}

	cd Example

	time ./runProguardJar.sh $1 proguardMOD.jar
	check_size proguardMOD.jar

	#time ./runProguardJar.sh $1 proguardMOD_REDEX_WRAPPERS.jar

	time ./runProguardJar.sh $1 proguard.jar
	check_size proguard.jar

	time ./runProguardJar.sh $1 proguardSTATIC.jar
	check_size proguardSTATIC.jar

	#time ./runProguardJar.sh $1 proguardSTATIC_REDEX_WRAPPERS.jar

	time ./runProguardJar.sh $1 proguardINLINE.jar
	check_size proguardINLINE.jar



