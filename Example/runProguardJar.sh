#!/bin/bash
# $1 -> times to execute the experiment
# $2 -> name of jar file


for i in $(eval echo {1..$1})
do
	java -jar ./$2 @ proguard.pro
	rm proguard.map
done

