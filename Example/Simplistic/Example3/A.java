public class A {

    public static void main(String[] args)
    {
        B b = new B();
        for (int i=0; i<99999; i++)
            b.foo();
    }
}

class B {

    public void foo() {
        bar();
    }

    private void bar() { System.out.println(5);
    }
}
