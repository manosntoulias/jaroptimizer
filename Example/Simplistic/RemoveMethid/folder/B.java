import java.util.HashSet;

public class B {

	public void foo(int i, float f) { int x= 5; System.out.println(x); return;}

	public void bar(int i, float f) { System.out.println(i); return;}

	public void foo(int i, float f, float f2) { System.out.println(i); return;}

	public String aboo(HashSet<String> set, String str) { return null;}

	public static void main(String[] Args) {

	B a = new B();

	a.bar(3,4);
	a.foo(5,6,7);
	a.foo(7,8);
}

}
