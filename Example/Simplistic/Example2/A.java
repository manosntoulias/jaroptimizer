import java.util.HashSet;
import java.util.jar.Attributes;

public class A extends Attributes {

	public void foo(int i, float f) { int x= 5; System.out.println(x); return;}

	public void bar(int i, float f) { if (i<3) return; System.out.println(i--); bar(i,f);}

	public void foo(int i, float f, float f2) { System.out.println(i); return;}

	public String aboo(HashSet<String> set, String str) { return null;}

	public void useProtectedFromExternal()
	{	map = null;
		System.out.println("Protected from external accessed");
	}

	public A over(A a) {
		//a.bar(4,4);
		try {	a.bar(4,4); }
		catch (Exception e)
		{	System.out.println(9);}
		return null;}

	public static void main(String[] Args) {

	
	int x = 4;
	int y = 3;
	int z = 7;
	int w = 6;
	int k = 8;


	A a = new A();

	a.bar(y,x);
	a.foo(x,w,z);
	a.foo(z,k);

	a.over(a);
	a = a.new A2();
	a.over(a);

	C c = new C2();
	c.over(4);

	C3 c3 = new C3();
	c3.bar(16);

	c3.far(a);
}

	class A2 extends A {

	private double y = 8.0;

	@Override
	public A2 over(A a) {

		((A2)a).rod(7.0);			
		System.out.println(10);

		long timer = System.currentTimeMillis();
		a.foo((timer % 2 == 0) ? 1 : 10,0);
		return null;

		}

	private double rod(double x) {System.out.println(y); return x-1;}

	@Override
	public void foo(int i, float f) { System.out.println("I'm foo A2: " + i);}

}
	


}

class C {
	
	public int over(int x) {

		this.rod(2.0);			
		System.out.println(x);
		return 5;

	}

	private double rod(double x) {System.out.println(x); return x-1;}

	

	public int bar(int x) {

		System.out.println(x + " in C"); return x-1;

	}
	
}

class C2 extends C {

	@Override
	public int over(int x) {

		System.out.println(3.0);			
		System.out.println(x+1);
		return 5;

	}

}


class C3 extends C2 {

	@Override
	public int bar(int x) {

		x--;
		return super.bar(x);

	}

	public void far(A a) { a.useProtectedFromExternal(); }

}

