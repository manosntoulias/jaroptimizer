#!/bin/bash
# $1 -> times to run the experiment
# $2 -> name of jar

sudo cp /usr/lib/jvm/java-8-openjdk-amd64/lib/$2 /usr/lib/jvm/java-8-openjdk-amd64/lib/tools.jar
for i in $(eval echo {1..$1})
do
        ant build-bootstrap-javac
        ant clean
done

