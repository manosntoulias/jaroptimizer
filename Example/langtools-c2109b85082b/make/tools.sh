#!/bin/bash

cd ../../..

java -cp build/libs/jaroptimizer-all.jar:Example/tools.jar optimiser.JarOptimiser -jar Example/tools.jar -o Example/langtools-c2109b85082b/make/toolsMOD.jar -i analysis/context_insensitive/javac/SingleInvocationToInline.csv -s analysis/context_insensitive/javac/SingleInvocationTarget.csv -u analysis/context_insensitive/javac/UnsoundInvocationTarget.csv -d 1 -p inline

java -cp build/libs/jaroptimizer-all.jar:Example/tools.jar optimiser.JarOptimiser -jar Example/tools.jar -o Example/langtools-c2109b85082b/make/toolsSTATIC.jar -s analysis/context_insensitive/javac/SingleInvocationTarget.csv -u analysis/context_insensitive/javac/UnsoundInvocationTarget.csv -d 1 -p inline

java -cp build/libs/jaroptimizer-all.jar:Example/tools.jar optimiser.JarOptimiser -jar Example/tools.jar -o Example/langtools-c2109b85082b/make/toolsINLINE.jar -i analysis/context_insensitive/javac/SingleInvocationToInline.csv -u analysis/context_insensitive/javac/UnsoundInvocationTarget.csv -d 1 -p inline


cd Example/langtools-c2109b85082b/make/

ant clean

time ./runToolJar.sh $1 toolsMOD.jar

#time ./runToolJar.sh $1 toolsMOD_THESIS.jar

time ./runToolJar.sh $1 toolsOG.jar

time ./runToolJar.sh $1 toolsINLINE.jar

time ./runToolJar.sh $1 toolsSTATIC.jar

#time ./runToolJar.sh $1 toolsSTATIC_THESIS.jar

sudo cp /usr/lib/jvm/java-8-openjdk-amd64/lib/toolsOG.jar /usr/lib/jvm/java-8-openjdk-amd64/lib/tools.jar
