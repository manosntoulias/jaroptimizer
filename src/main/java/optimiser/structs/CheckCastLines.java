package optimiser.structs;

import java.util.HashMap;

public class CheckCastLines extends HashClass<HashMap<String,HashMap<Integer, String>>> {

	/* return this empty map when method with sig "method_sig" belonging in class "className"
	 * doesn't have any optimizations to be made */
	HashMap<Integer, String> emptyCallSites;
	
	public CheckCastLines() {
		super();
		emptyCallSites = new HashMap<Integer, String>(0);
	}

	

	@Override
	public boolean containsKey(String className, String method_sig) {
		if (!this.containsKey(className))
			return false;
		
		if (this.get(className).containsKey(method_sig))
			return true;
		
		return false;
	}

	@Override
	public HashMap<Integer, String> get(String className, String method_sig) {
		HashMap<String, HashMap<Integer, String>> classMap;
		HashMap<Integer, String> callsites;
		if ((classMap = this.get(className)) == null ||
			(callsites = classMap.get(method_sig)) == null)
				return emptyCallSites;
			
		return callsites;
	}
	
	@Override
	protected Object set_method(String inside_class, String return_type, String method_name, String arguments) {
		
		return null;
	}

}
