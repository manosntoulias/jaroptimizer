package optimiser.structs;

import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.MatchResult;

import optimiser.parsers.FileParser;

public class CallSites extends HashClass<HashMap<String, HashMap<String, String[]>>>
{
		/* return this empty map when method with sig "method_sig" belonging in class "className"
		 * doesn't have any optimizations to be made */
		HashMap<String, String[]> emptyCallSites;
		
		
		public CallSites()
		{
			super();
			emptyCallSites = new HashMap<String, String[]>(0);
		}
	
		@Override
		public HashMap<String, String[]> set_method(String inside_class, String return_type, String method_name, String arguments)
		{
			inside_class = inside_class.replaceAll("\\.","/");			
			if (!containsKey(inside_class))
			{
				put(inside_class, new HashMap<String, HashMap<String, String[]>>());
			}
			
			HashMap<String, HashMap<String, String[]>> set = get(inside_class);
			
			//NOTE: FileParser.new_descr_matcher(); must have been called first
			String sig = method_name + "\t" + FileParser.create_descriptor(arguments, return_type);
			if (!set.containsKey(sig))
				set.put(sig, new HashMap<String, String[]>());
			
			return set.get(sig);
			
		}

		@Override
		public boolean containsKey(String className, String method_sig) {
			
			if (!this.containsKey(className))
				return false;
			
			if (this.get(className).containsKey(method_sig))
				return true;
			
			return false;
		}
		
		@Override
		public HashMap<String, String[]> get(String className, String method_sig) {
			
			HashMap<String, HashMap<String, String[]>> classMap;
			HashMap<String, String[]> callsites;
			if ((classMap = this.get(className)) == null ||
				(callsites = classMap.get(method_sig)) == null)
					return emptyCallSites;
				
			return callsites;
			
		}
		
		
		protected String[] remove(String className, String method_sig, String callSite) {
			
			HashMap<String, String[]> callSites =  get(className, method_sig);
			
			String[] retValue = callSites.remove(callSite);
			
			if (retValue != null && callSites.isEmpty())
			{
				HashMap<String, HashMap<String, String[]>> classMap = this.get(className);
				if (classMap.remove(method_sig) != null && classMap.isEmpty())
					this.remove(className);
			}
			
			return retValue;
		}
		
		public void removeUnsoundCallsites(CallSites unsound) {
			
			Iterator<Entry<String, HashMap<String, HashMap<String, String[]>>>> class_iter = unsound.entrySet().iterator();
			
			while (class_iter.hasNext())
			{
				Entry<String, HashMap<String, HashMap<String, String[]>>> entry = class_iter.next();
				String className = entry.getKey();
				Iterator<Entry<String, HashMap<String, String[]>>> meth_iter = entry.getValue().entrySet().iterator();
				
				while (meth_iter.hasNext())
				{
					Entry<String, HashMap<String, String[]>> meth_entry = meth_iter.next();
					String methName = meth_entry.getKey();
					Iterator<Entry<String, String[]>> callSite_iter = meth_entry.getValue().entrySet().iterator();
					
					while (callSite_iter.hasNext())
					{
						Entry<String, String[]> callSite_entry = callSite_iter.next();
						String callSite = callSite_entry.getKey();
						
						remove(className, methName, callSite);
						
					}
				}
			}
			
			
		}


}