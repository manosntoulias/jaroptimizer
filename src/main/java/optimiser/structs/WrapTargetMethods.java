package optimiser.structs;

import java.util.HashMap;
import java.util.HashSet;

import optimiser.parsers.FileParser;

public class WrapTargetMethods extends HashClass<HashMap<String,String>>
{

		
		public WrapTargetMethods()
		{
			super();
		}
	
		@Override
		public Object set_method(String inside_class, String return_type, String method_name, String arguments)
		{
			inside_class = inside_class.replaceAll("\\.","/");
			if (!containsKey(inside_class))
			{
				put(inside_class, new HashMap<String,String>());
			}
			
			HashMap<String,String> set = get(inside_class);
			
			//NOTE: FileParser.new_descr_matcher(); must have been called first
			String sig = method_name + "\t" + FileParser.create_descriptor(arguments, return_type);
			set.put(sig, FileParser.match_class_descriptor(inside_class));
			
			return null;
			
		}


		@Override
		public boolean containsKey(String className, String method_sig) {
			
			if (!this.containsKey(className))
				return false;
			
			if (this.get(className).containsKey(method_sig))
				return true;
			
			return false;
		}
		
		@Override
		public String get(String className, String method_sig) {
			
			HashMap<String, String> map;
			if ((map = this.get(className)) == null)
				return null;
			
			return map.get(method_sig);
			
		}


}