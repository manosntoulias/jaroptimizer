package optimiser.visitors;

import java.util.HashMap;

public interface CallSiteHelper {

	public default String getCallSite(HashMap<String,Integer> method_callsites, String owner, String name) {
		
		String method_call = owner + "/" + name;
		if (!method_callsites.containsKey(method_call))
			method_callsites.put(method_call, 0);
		
		int call_counter = method_callsites.get(method_call);
		
		String callSite = method_call + "\t" + call_counter;
		method_callsites.put(method_call, ++call_counter);
		
		return callSite;
	}

}
