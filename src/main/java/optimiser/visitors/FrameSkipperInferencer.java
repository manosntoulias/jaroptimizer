package optimiser.visitors;

import java.util.ArrayList;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/*
 * A TypeInferencer that finds the frames of a class without using stack map frames
 * This means that we don't know the exact types of the frames
 * But we do know the exact length of the operand stack and local variables
 */


public class FrameSkipperInferencer extends TypeInferencer {

	public FrameSkipperInferencer(String owner, int access, String name, String descriptor,
			MethodVisitor methodVisitor) {
		super(owner, access, name, descriptor, methodVisitor);
		// TODO Auto-generated constructor stub
	}
	
	//DO NOTHING -> skips the frame
	@Override
	public void visitFrame(
		      final int type,
		      final int numLocal,
		      final Object[] local,
		      final int numStack,
		      final Object[] stack) {
		
	}
	
	
	@Override
	  public void visitJumpInsn(final int opcode, final Label label) {
		
		super.visitJumpInsn(opcode, label);
		
		  // IF THE LABELS OF THE NEXT INSTRUCTION DON'T SPECIFY A STACK
		  //IT'S A CATCH BLOCK (AS FAR AS I KNOW)
		  //OTHERWISE THE LABELS FOLLOWING THIS GOTO WILL OVERRIDE THIS SAFE MECHANISM
		  //SAME FOR ATHROW AND RETURN INSTRUCTIONS
		  if (opcode == Opcodes.GOTO) {
			  stack = new ArrayList<Object>();
			  locals = new ArrayList<Object>();
			  stack.add("java/lang/Exception");
		  }
	}
	
	@Override
	public void visitInsn(final int opcode) {

		super.visitInsn(opcode);
		  
		//SEE GOTO INSTRUCTION
		if ((opcode >= Opcodes.IRETURN && opcode <= Opcodes.RETURN) || opcode == Opcodes.ATHROW) {
			stack = new ArrayList<Object>();
			locals = new ArrayList<Object>();
			stack.add("java/lang/Exception");
		}
		
		
	    
	}


}
