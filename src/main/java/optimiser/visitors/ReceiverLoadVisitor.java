package optimiser.visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.objectweb.asm.ConstantDynamic;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.AnalyzerAdapter;
import org.objectweb.asm.tree.InsnList;

import optimiser.JarOptimiser;
import optimiser.structs.ClassHierarchy;
import optimiser.utils.ObjectRef;


//MAKE SURE NEXT VISITOR METHODS ARE CALLED IN CASE A METHOD NODE FOLLOWS. OTHERWISE INSTRUCTIONS WILL BE LOST DURING INLINE!
public class ReceiverLoadVisitor extends TypeInferencer implements CallSiteHelper {

	private int nextObjectRef;
	
	private boolean mergeRef;
	
	
	HashMap<ObjectRef, Integer> ReferenceAtTopLines;
	
	
	private HashMap<String, String[]> virtualToStatic;
	private HashMap<String, String[]> inlineCallSites;
	private HashMap<String, Integer> method_callsites; 
	private HashMap<Integer, String> checkcast_lines;
	private ClassHierarchy hierarchy;
	
	
	protected LineNumberVisitor lnv;
	
	
	String methName;
	String insideClass;
	String superName;
	String descriptor;

	JarOptimiser jarOpt;

	public ReceiverLoadVisitor(int api, JarOptimiser jarOpt, String owner, String superName, int access, String name, String descriptor,
			MethodVisitor methodVisitor, HashMap<Integer, String> checkcast_lines) {
		super(owner, access, name, descriptor, methodVisitor);

		this.jarOpt = jarOpt;
		
		ReferenceAtTopLines = new HashMap<ObjectRef, Integer>();
		
		nextObjectRef = 0;
		
		virtualToStatic = jarOpt.getSingleInvocs().get(owner, name + "\t" + descriptor);
		inlineCallSites = jarOpt.getSingleInvocs_inline().get(owner, name + "\t" + descriptor);
		method_callsites = new HashMap<String,Integer>();
		this.checkcast_lines = checkcast_lines;
		this.hierarchy = jarOpt.getHierarchy();
		
		

		
		
		methName = name;
		this.insideClass = owner;
		this.superName = superName;
		this.descriptor = descriptor;
		this.mergeRef = false;
	}
	
	
	
	private void visitInsn1(int opcode) {

		//System.out.println("\n"+opcode + " Opcode. name: "+ methName+descriptor);
		//printStack();
		  
		
		if (stack != null && !stack.isEmpty())
		{
			int i = stack.size()-1;
			int head = stack.size()-1;
			
			Object type = stack.get(i);
			if (type instanceof String)
			{
				String str_type = (String)type;
				if (str_type.charAt(0) != '[')
				{
					ObjectRef objRef = new ObjectRef(nextObjectRef, stack.remove(head));
					stack.add(objRef);
			    	ReferenceAtTopLines.put(objRef, lnv.getLineNumber());
			    	nextObjectRef++;  	
				}
			}
			else if (type instanceof ObjectRef && mergeRef)
			{
				ObjectRef objRef = (ObjectRef)type;
				ReferenceAtTopLines.put(objRef, lnv.getLineNumber());
				
			}
			
		} 
		mergeRef = false;
		    

	}
	
	
	@Override
	  public void visitMethodInsn(
	      final int opcode,
	      final String ownr,
	      final String name,
	      final String descriptor,
	      final boolean isInterface) {
		
		visitInsn1(opcode);
		
		String owner = ownr;
		
		if (opcode == Opcodes.INVOKEVIRTUAL || opcode == Opcodes.INVOKEINTERFACE)
		{				
			String callSite = getCallSite(method_callsites, owner, name);

			if (virtualToStatic.containsKey(callSite) || inlineCallSites.containsKey(callSite))
			{
				Type[] descrArray = Type.getArgumentTypes(descriptor);
				int receiverOffset = descrArray.length;
				for (int i=0; i<descrArray.length; i++)
					if (descrArray[i].equals(Type.LONG_TYPE) || descrArray[i].equals(Type.DOUBLE_TYPE))
						receiverOffset++;
				Object obj = stack.get(stack.size()-1 -receiverOffset);
				
				owner = getOperandString(obj);
				
				String dynamicReceiver = StaticOrInline(callSite);

				Boolean needsCheckcast = false;
				if ((hierarchy.isStrictSubTypeOf(dynamicReceiver, owner) || !hierarchy.isSubTypeOf(owner, dynamicReceiver)))
					needsCheckcast = true;
								
				if (needsCheckcast)
				{
					if (obj instanceof ObjectRef)
					{	
						ObjectRef objRef = (ObjectRef)obj;
						int line = ReferenceAtTopLines.get(objRef);
						if (checkcast_lines.containsKey(line))
						{	if (hierarchy.isStrictSubTypeOf(dynamicReceiver, checkcast_lines.get(line)))
								checkcast_lines.put(line, dynamicReceiver);	
						}
						else
							checkcast_lines.put(line, dynamicReceiver);
					}
					// else safe mechanism. 
					//remove devirtualization of callsite if obj is not ObjectRef (and hence we dont know where to place checkcast)
					else 
					{	virtualToStatic.remove(callSite);
						inlineCallSites.remove(callSite);
						jarOpt.incDevirt_removed();
					}
				}				
				 
			}
			
		}
		
		
		super.visitMethodInsn(opcode, ownr, name, descriptor, isInterface);
		//visitInsn1(opcode);
	}
	
	private String getOperandString(Object type)
	{
		if (type instanceof String)
			return (String)type;
		else if (type instanceof ObjectRef)
			return (String)((ObjectRef)type).getStaticRef();
		return null;
	}
	
	private String StaticOrInline(String callSite) {
		
		String[] ownerANDretType;
		
		if ((ownerANDretType = virtualToStatic.get(callSite)) == null)
			ownerANDretType = inlineCallSites.get(callSite);
			
		return ownerANDretType[0];
		
	}
	
	
	//We don't need to use any frames
	@Override
	  public void visitFrame(
	      final int type,
	      final int numLocal,
	      final Object[] local,
	      final int numStack,
	      final Object[] stack) {
		
		if (this.stack != null)
		{
			//System.out.println("Frame " + this.stack);
			
			List<Object> stack_temp = new ArrayList<Object>(this.stack);
			List<Object> locals_temp = new ArrayList<Object>(this.locals);
			super.visitFrame(type, numLocal, local, numStack, stack);
			
			//System.out.println("Frame " + this.stack);
			  
			
			updateObjectRefs(stack_temp, this.stack);
			updateObjectRefs(locals_temp, this.locals);
			
			//System.out.println("Frame " + this.stack);
		}
		else
			super.visitFrame(type, numLocal, local, numStack, stack);
	    
	  }
	
	private static void updateObjectRefs(
		      final List<Object> source, final List<Object> target) {
		
		for (int i = 0; i<target.size(); i++) {
			
			Object OperandType = source.get(i);
		    if (OperandType instanceof ObjectRef)
		    {	ObjectRef objRef = (ObjectRef) OperandType;
		    	objRef.setStaticRef(target.get(i));
		    	target.set(i, objRef);
		    }
		}
	}
	
	@Override
	public void visitLabel(final Label label) {
		
		if (this.stack != null && !this.stack.isEmpty() && stacks.containsKey(label))
		{
			int head = this.stack.size() - 1;
			List<Object> labelStack = stacks.get(label);
			Object labelObj = labelStack.get(head);
			Object obj = this.stack.get(head);
			if (labelObj instanceof ObjectRef && obj instanceof ObjectRef)
			{
				if (!labelObj.equals(obj))
					mergeRef = true;
			}
		}
		
		super.visitLabel(label);
	}


	@Override
	public void visitInsn(final int opcode) {

		visitInsn1(opcode);
		super.visitInsn(opcode);
		
		//if ((opcode < Opcodes.IRETURN || opcode > Opcodes.RETURN) && opcode != Opcodes.ATHROW)
			//visitInsn1(opcode);
	    
	}

	  @Override
	  public void visitIntInsn(final int opcode, final int operand) {
		  	visitInsn1(opcode);
		  	super.visitIntInsn(opcode, operand);
	  }

	  @Override
	  public void visitVarInsn(final int opcode, final int var) {
		  visitInsn1(opcode);
		  super.visitVarInsn(opcode, var);
	  }

	  @Override
	  public void visitTypeInsn(final int opcode, final String type) {
		  visitInsn1(opcode);
		  super.visitTypeInsn(opcode, type);
	  }

	  @Override
	  public void visitFieldInsn(
	      final int opcode, final String owner, final String name, final String descriptor) {
		visitInsn1(opcode);
	    super.visitFieldInsn(opcode, owner, name, descriptor);
	    
	  }

	  

	  

	  

	  @Override
	  public void visitInvokeDynamicInsn(
	      final String name,
	      final String descriptor,
	      final Handle bootstrapMethodHandle,
	      final Object... bootstrapMethodArguments) {
		  visitInsn1(Opcodes.INVOKEDYNAMIC);
		  super.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
	  }



	  @Override
	  public void visitLdcInsn(final Object value) {
		  visitInsn1(Opcodes.LDC);
		  super.visitLdcInsn(value);
	  }

	  @Override
	  public void visitIincInsn(final int var, final int increment) {
		  visitInsn1(Opcodes.IINC);
		  super.visitIincInsn(var, increment);
	  }


	  @Override
	  public void visitMultiANewArrayInsn(final String descriptor, final int numDimensions) {
		  visitInsn1(Opcodes.MULTIANEWARRAY);
		  super.visitMultiANewArrayInsn(descriptor, numDimensions);
	  }

	  @Override
	  public void visitLocalVariable(
	      final String name,
	      final String descriptor,
	      final String signature,
	      final Label start,
	      final Label end,
	      final int index) {
		  
		  super.visitLocalVariable(name, descriptor, signature, start, end, index);
		  
		  
	  }

	  @Override
	  public void visitMaxs(final int maxStack, final int maxLocals) {
		  super.visitMaxs(maxStack, maxLocals);
	  }

		public void setLnv(LineNumberVisitor lnv) {
			this.lnv = lnv;
		}
	  
	  public void printStack()
	  {
		  //System.out.println("\nSize: " + stack.size());
		  for (int i=0; i< stack.size(); i++)
			  System.out.println(stack.get(i));
	  }
}
