package optimiser.visitors;


import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;


public class LineNumberVisitor extends MethodVisitor {

	protected int lineNumber;

		

	
	public LineNumberVisitor(MethodVisitor methodVisitor, int startingLine) {
		super(Opcodes.ASM7, methodVisitor);
		
		lineNumber = startingLine;
			
	}
	
	@Override
	public void visitMethodInsn(int opcode, java.lang.String owner, java.lang.String name, java.lang.String descriptor, boolean isInterface)
	{	
		super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
		lineNumber++;
	}
	

	@Override
	  public void visitInsn(final int opcode) {
		  super.visitInsn(opcode);
		  lineNumber++;
	    
	  }

	  @Override
	  public void visitIntInsn(final int opcode, final int operand) {
		  	super.visitIntInsn(opcode, operand);
		  	lineNumber++;
	  }

	  @Override
	  public void visitVarInsn(final int opcode, final int var) {
		  super.visitVarInsn(opcode, var);
		  lineNumber++;
	  }

	  @Override
	  public void visitTypeInsn(final int opcode, final String type) {
		  super.visitTypeInsn(opcode, type);
		  lineNumber++;
	  }

	  @Override
	  public void visitFieldInsn(
	      final int opcode, final String owner, final String name, final String descriptor) {
	    super.visitFieldInsn(opcode, owner, name, descriptor);
	    lineNumber++;
	    
	  }


	  @Override
	  public void visitInvokeDynamicInsn(
	      final String name,
	      final String descriptor,
	      final Handle bootstrapMethodHandle,
	      final Object... bootstrapMethodArguments) {
		  super.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
		  lineNumber++;
	  }

	  @Override
	  public void visitJumpInsn(final int opcode, final Label label) {
		  super.visitJumpInsn(opcode, label);
		  lineNumber++;
	  }


	  @Override
	  public void visitLdcInsn(final Object value) {
		  super.visitLdcInsn(value);
		  lineNumber++;
	  }

	  @Override
	  public void visitIincInsn(final int var, final int increment) {
		  super.visitIincInsn(var, increment);
		  lineNumber++;
	  }

	  @Override
	  public void visitTableSwitchInsn(
	      final int min, final int max, final Label dflt, final Label... labels) {
		  super.visitTableSwitchInsn(min, max, dflt, labels);
		  lineNumber++;
	  }

	  @Override
	  public void visitLookupSwitchInsn(final Label dflt, final int[] keys, final Label[] labels) {
		  super.visitLookupSwitchInsn(dflt, keys, labels);
		  lineNumber++;
	  }

	  @Override
	  public void visitMultiANewArrayInsn(final String descriptor, final int numDimensions) {
		  super.visitMultiANewArrayInsn(descriptor, numDimensions);
		  lineNumber++;
	  }

	
	  	public void setMv(MethodVisitor mv)
	  	{
	  		this.mv = mv;
	  	}
	
		public int getLineNumber() {
			return lineNumber;
		}
		
		public void incrementLine()
		{
			lineNumber++;
		}
}
