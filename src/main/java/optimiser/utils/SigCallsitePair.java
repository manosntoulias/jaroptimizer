package optimiser.utils;

public class SigCallsitePair {

	private String sig;
	private String callSite;
	
	public SigCallsitePair(String sig, String callSite) {
		this.sig = sig;
		this.callSite = callSite;		
	}
	
	public void set(String sig, String callSite) {
		this.sig = sig;
		this.callSite = callSite;		
	}


	public String getSig() {
		return sig;
	}


	public String getCallSite() {
		return callSite;
	}

	
}