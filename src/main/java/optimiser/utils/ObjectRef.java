package optimiser.utils;

public class ObjectRef {
	  
	  int id;
	  Object staticRef;
	  
	  public ObjectRef(int id, Object staticRef) {
		  
		  this.id = id;
		  this.staticRef = staticRef;
	  }
	  
	  @Override
	  public int hashCode()
	  {
		  return id;
	  }
	  
	  @Override
	  public boolean equals(Object obj)
	  {
		  if (!(obj instanceof ObjectRef))
			  return false;
		  
		  if (((ObjectRef)obj).getId() == this.id)
			  return true;
		  return false;
	  }
	  
	  @Override
	  public String toString() {
		  return "ObjRef(" + staticRef + ")";
	  }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Object getStaticRef() {
		return staticRef;
	}

	public void setStaticRef(Object staticRef) {
		this.staticRef = staticRef;
	}
	  
	  
}
