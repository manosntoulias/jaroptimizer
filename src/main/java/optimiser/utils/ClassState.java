package optimiser.utils;

public class ClassState {

	private boolean inlining; 
	
	public ClassState(boolean inlining) {
		this.inlining = inlining;
	}

	public boolean isInlining() {
		return inlining;
	}

	public void setInlining(boolean inlining) {
		this.inlining = inlining;
	}
	
	
	

}
