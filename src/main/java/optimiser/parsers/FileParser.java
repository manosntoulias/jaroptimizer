package optimiser.parsers;

import java.io.BufferedReader;
import java.io.CharArrayReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.regex.Matcher;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.Scanner;
import java.util.HashMap;

//<\w+(\.\w+)*(\$\w+)*:\s+\w+(\.\w+)*(\$\w+)*\s+(<init>|\w+)\((\w+(\.\w+)*(\$\w+)*(,\w+(\.\w+)*(\$\w+)*)*)?\)>
public abstract class FileParser extends BufferedReader {

	//private String filename;
	static protected HashMap<String, String> primitive_descriptors;
	static protected Pattern matrix_pattern;
	protected Pattern format_pattern;


	//each line must start with < and end with >
	//have a : right after the class that the method belongs to
	//$ symbol is for inner classes
	//[] for matrixes. return types and args can be matrixes
	//<class: return_type meth_name(args)>
	protected final String CORRECT_FORMAT;// = "<\\w+(\\.\\w+)*(\\$\\w+)*:\\s+\\w+(\\.\\w+)*(\\$\\w+)*(\\[\\])?\\s+(<init>|\\w+)\\((\\w+(\\.\\w+)*(\\$\\w+)*(\\[\\])?(,\\w+(\\.\\w+)*(\\$\\w+)*(\\[\\])?)*)?\\)>";

	protected static final String METHOD_FORMAT = "<(\\S+):\\s+(\\S+)\\s+(\\S+)\\((.*)\\)>";
	
	public FileParser(String filename, String format) throws FileNotFoundException {
		super(new FileReader(filename));
		
		
		CORRECT_FORMAT = format;
		format_pattern = Pattern.compile(CORRECT_FORMAT);
		
	}
	
	public FileParser() {
		super(new CharArrayReader(new char[0]));

		CORRECT_FORMAT = null;
		format_pattern = null;
	}
	


	
	public void parseAll() throws IOException {

		String line;
		
		while ((line = readLine()) != null) {

			parse(line);
			
		}
	

	}


	abstract protected void parse(String line);

	protected MatchResult scan(String line, String format)
	{
		Scanner s = new Scanner(line);
		s.findInLine(format);
     	MatchResult result = s.match();
     	s.close();
     	return result;
		
		
	}
	/*
	protected H parse_method(MatchResult result, HashMap<String, HashMap<String, H>> map, int index)
	{
		String inside_class = result.group(index).replaceAll("\\.","/") + ".class";
		String return_type = result.group(index+1);
		String method_name = result.group(index+2);
		String arguments = result.group(index+3);
		
		if (!map.containsKey(inside_class))
		{
			map.put(inside_class, new HashMap<String, H>());
		}
		
		HashMap<String, H> set = map.get(inside_class);
		
		String sig = method_name + "\t" + create_descriptor(arguments, return_type);
		if (!set.containsKey(sig))
			set.put(sig, create_hashStruct());
		
		return set.get(sig);
		
	}*/

	public static String create_descriptor(String arguments, String return_type) {

		String meth_descriptor = "(";		

		//RETURN DESCRIPTOR
		String ret_descriptor = match_descriptor(return_type);

		//NO ARGUMENTS	
		if (arguments.equals(""))
			return "()" + ret_descriptor;
		
		//WITH ARGUMENTS
		String[] args = arguments.split(",");
		for (int i=0; i<args.length; i++)
			meth_descriptor += match_descriptor(args[i]);
	
		return meth_descriptor + ")" + ret_descriptor;

	}

	public static String match_class_descriptor(String type)
	{	
		return "L" + type.replaceAll("\\.","/") + ";";
	}
	
	protected static String match_descriptor(String type)
	{

		//CHECK IF IT IS AN ARRAY
		int num_of_brackets = 0;
		Matcher matcher = matrix_pattern.matcher(type);
		if (matcher.matches())
		{
			String[] bracketless_type = type.split("\\[");
			type = bracketless_type[0];
			num_of_brackets = bracketless_type.length-1;	
		}

		//CHECK IF IT IS A CLASS
		String descriptor = null;
		if ((descriptor = primitive_descriptors.get(type)) == null)
		{
			descriptor = "L" + type.replaceAll("\\.","/") + ";";
		}
		

		//IF IT IS AN ARRAY
		while (num_of_brackets > 0)
		{
			descriptor = "[" + descriptor;
			num_of_brackets--;
		}

		return descriptor;

		

	}
	
	public static String internal_name(String name)
	{ return name.replaceAll("\\.","/");}
	
	public static String uninternal_name(String name)
	{ return name.replaceAll("/","\\.");}


	public static void new_descr_matcher() {

		matrix_pattern = Pattern.compile(".*\\[\\]");
		primitive_descriptors = new HashMap<String, String>();

		primitive_descriptors.put("boolean", "Z");
		primitive_descriptors.put("char", "C");
		primitive_descriptors.put("byte", "B");
		primitive_descriptors.put("short", "S");
		primitive_descriptors.put("int", "I");
		primitive_descriptors.put("float", "F");
		primitive_descriptors.put("long", "J");
		primitive_descriptors.put("double", "D");
		primitive_descriptors.put("void", "V");
		//Object Ljava/lang/Object;
		//int[] [I
		//Object[][] [[Ljava/lang/Object;

		//return descriptors;

	}





} 
